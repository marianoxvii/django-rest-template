#!/bin/sh
#
# Flags:
#   -s --staging : Build for staging
#   -p --prod    : Build for production
#

set -e # Terminate script if one command fails

while (( "$#" )); do
  case "$1" in
    -s|--staging)
      ENV_FILE=.env.staging
      COMPOSE_FILE=docker-compose-staging.yml
      PROXY_CONF_FILE=proxy/default.conf.staging
      ARCHIVE_NAME=<my_project>-staging
      shift
      ;;
    -p|--prod)
      ENV_FILE=.env.prod
      COMPOSE_FILE=docker-compose-prod.yml
      PROXY_CONF_FILE=proxy/default.conf.prod
      ARCHIVE_NAME=<my_project>-prod
      shift
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      INPUT="$INPUT $1"
      shift
      ;;
  esac
done

ARCHIVES_PATH=./archives
OUTPUT_PATH=$ARCHIVES_PATH/$ARCHIVE_NAME
OUTPUT_PROXY_PATH=$OUTPUT_PATH/proxy

rm -rf $OUTPUT_PATH
mkdir -p $OUTPUT_PATH
mkdir -p $OUTPUT_PROXY_PATH

cp -rf <my_project> $OUTPUT_PATH
cp -rf $PROXY_CONF_FILE $OUTPUT_PROXY_PATH/default.conf
cp -rf proxy/Dockerfile $OUTPUT_PROXY_PATH
cp -rf proxy/uwsgi_params $OUTPUT_PROXY_PATH
cp -rf scripts $OUTPUT_PATH
cp -rf postgres $OUTPUT_PATH
cp -rf $ENV_FILE $OUTPUT_PATH/.env
cp -rf $COMPOSE_FILE $OUTPUT_PATH/docker-compose.yml
cp -rf Dockerfile $OUTPUT_PATH
cp -rf requirements.txt $OUTPUT_PATH

cd $ARCHIVES_PATH

tar -czf $ARCHIVE_NAME.tar.gz $ARCHIVE_NAME

rm -rf $ARCHIVE_NAME
