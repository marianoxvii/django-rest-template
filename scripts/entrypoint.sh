#!/bin/sh

set -e

python manage.py migrate

python manage.py ensure_adminuser --username=admin --password=$DJANGO_SUPERUSER_PASSWORD

if [ "$DEBUG" -eq "1" ]; then
	python manage.py runserver 0.0.0.0:8000
else
	python manage.py collectstatic --no-input
	uwsgi --socket :8000 --master --enable-threads --module <my_project>.wsgi
fi
