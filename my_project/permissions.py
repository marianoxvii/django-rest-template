from rest_framework import permissions
import os

class SecretKeyPermission(permissions.BasePermission):
    message = 'Secret key not provided or is not valid'

    def has_permission(self, request, view):
        secret_key_header = "X-Secret-Key"

        if secret_key_header not in request.headers:
            return False

        given_key = request.headers[secret_key_header]

        return given_key == os.environ.get('API_SECRET_KEY')
