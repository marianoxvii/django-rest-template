from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException
from rest_framework.serializers import ValidationError
from rest_framework.response import Response

def custom_exception_handler(exc, context):
    if isinstance(exc, ValidationError):
        data = {
            "code": "validation",
            "detail": exc.get_codes()
        }
        return Response(data = data, status = 400)

    if isinstance(exc, APIException):
        data = {
            "code": exc.get_codes(),
            "detail": exc.detail
        }
        return Response(data = data, status = exc.status_code)

    return exception_handler(exc, context)
