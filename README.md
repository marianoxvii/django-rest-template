# Django REST Template

## Create Python env

Create a new python environment

```
virtualenv env
```

Activate the new environment

```
source env/bin/activate
```

## Install dependencies

Copy `requirements.txt` to your project directory and install dependencies using

```
pip install -r requirements.txt
```

## Generate Django project and app

Generate the project. Change `my_project` to whatever name you want.

```
django-admin startproject my_project
```

Generate the app named `api`. You will need to `cd` into the django project directory first.

```
cd my_project
python manage.py startapp api
cd ..
```

Copy the contents of `api` directory into `my_project/api`.

Copy the contents of `my_project` directory into `my_project/my_project`. Remember to change all appearances of `<my_project>` to the name you used to generate the project (You can perform a search and replace).

Now you can test the installation by running the server.

```
python my_project/manager.py runserver
```

The `.gitignore.django` file contains specifications for django projects. Copy it into `my_project` directory (or the name you used for your django project) and rename it to `.gitignore`.

## Setup .env

Copy `.env.sample` to your project root directory and duplicate it to generate the environment files for the different builds:
- `.env` for dev build.
- `.env.staging` for staging build.
- `.env.prod` for production build.

Remember to change all appearances of `<my_project>` to the name you used to generate the project (You can perform a search and replace).

Also copy `.gitignore` file to your project root directory to avoid checking out the `.env`, `.env.staging` and `.env.prod` files into version control.

## Update project settings

Copy the contents of `settings.py` into `my_project/my_project/settings.py` and remember to change all appearances of `<my_project>` to the name you used to generate the project (You can perform a search and replace).

## Setup Docker

Copy the following files and directories into your project's root directory:

- `Dockerfile` (File)
- `docker-compose.yml` (File)
- `docker-compose-staging.yml` (File)
- `docker-compose-prod.yml` (File)
- `archive.sh` (File)
- `proxy` (Dir)
- `scripts` (Dir)
- `postgres` (Dir)

Replace all appearances of `<my_project>` to the name you used to generate the django project in the following files (You can perform a search and replace):

- `Dockerfile`
- `docker-compose.yml`
- `docker-compose-staging.yml`
- `docker-compose-prod.yml`
- `archive.sh`
- `proxy/default.conf.prod`
- `proxy/default.conf.staging`
- `scripts/entrypoint.sh`

You can now test the project using docker compose (It is run on port 8000 now). Make sure to update the values related to postgres and the database in your `.env` file before running the containers.

```
docker-compose up --build -d my_project-db-dev
docker-compose up --build -d my_project-dev
```

Make sure `my_project-db-dev` container is up and running before starting `my_project-dev` container, otherwise it will fail.

## Deployment

You can use the `archive.sh` script to bundle the project files for `staging` or `production` build.

Use `./archive.sh -s` for `staging` build.

Use `./archive.sh -p` for `production` build.

This script will generate a tar file that you can deploy to a server, uncompress and manage using docker compose.

The script generates a single `docker-compose.yml` and `.env` file from the specific build type files, so make sure that `docker-compose.prod.yml`, `docker-compose.staging.yml`, `.env.staging`, `.env.prod` files exist before running the command.
