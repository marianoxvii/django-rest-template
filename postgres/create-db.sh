#!/bin/bash

set -e # Terminate script if one command fails

psql -U postgres -c "CREATE USER $DB_USER WITH PASSWORD '$DB_PASSWORD';"
createdb -U postgres $DB_NAME
psql -U postgres -c "ALTER DATABASE $DB_NAME OWNER TO $DB_USER;"